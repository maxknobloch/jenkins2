import numpy as np 
import matplotlib.pyplot as plt 


def f(x): 
	return x**2 

def integrator(a,b): # a,b Integralgrenzen, y Funktion
	x = np.arange(a,b,0.0001) # Intervall 
	c = 0 # Zahler 
	g = 0 
	while(c < len(x)-3): 
		g = g + (x[c+2]-x[c])*f(x[c+1])
		c += 2 
	return g 

a = 0 #Intervall Anfang 
b = 2 #Intervall Ende 

print(integrator(a,b))







